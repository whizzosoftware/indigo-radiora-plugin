#! /usr/bin/env python
# -*- coding: utf-8 -*-
####################
# Lutron RadioRA server plugin
#
# Copyright (c) 2012, Whizzo Software, LLC. All rights reserved.
# http://www.whizzosoftware.com

from __future__ import with_statement

import functools
import os
import serial
import sys
import threading
import time
import indigo

RA_PHANTOM_BUTTON = "raPhantomButton"
RA_DIMMER = "raDimmer"
PROP_BUTTON = "button"
PROP_ZONE = "zone"

################################################################################
class Plugin(indigo.PluginBase):
	########################################
	def __init__(self, pluginId, pluginDisplayName, pluginVersion, pluginPrefs):
		indigo.PluginBase.__init__(self, pluginId, pluginDisplayName, pluginVersion, pluginPrefs)

		if 'debugEnabled' in pluginPrefs:
			self.debug = pluginPrefs['debugEnabled']
		else:
			self.debug = False

		self.conn = {}
		self.command = ''
		self.phantomButtons = {}
		self.zones = {}
		self.lastBrightness = {}
		
	def __del__(self):
		indigo.PluginBase.__del__(self)

	def startup(self):
		self.debugLog(u"startup called")
		self.portEnabled = False

		serialUrl = self.getSerialPortUrl(self.pluginPrefs, u"devicePort")
		self.debugLog(u"Serial Port URL is: " + serialUrl)

		self.conn = self.openSerial(u"Lutron RadioRA", serialUrl, 9600, stopbits=1, timeout=2, writeTimeout=1)
		if self.conn is None:
			indigo.server.log(u"Failed to open serial port")
			return

		self.portEnabled = True

	def deviceStartComm(self, dev):
		if dev.deviceTypeId == RA_PHANTOM_BUTTON:
			self.phantomButtons[dev.pluginProps[PROP_BUTTON]] = dev
			self.debugLog(u"Found phantom button: " + dev.pluginProps[PROP_BUTTON])
		elif dev.deviceTypeId == RA_DIMMER:
			self.zones[dev.pluginProps[PROP_ZONE]] = dev
			self.debugLog(u"Found dimmer: " + dev.pluginProps[PROP_ZONE])

	def deviceStopComm(self, dev):
		if dev.deviceTypeId == RA_PHANTOM_BUTTON:
			del self.phantomButtons[dev.pluginProps[PROP_BUTTON]]
			self.debugLog(u"Removed phantom button: " + dev.pluginProps[PROP_BUTTON])
		elif dev.deviceTypeId == RA_DIMMER:
			del self.zones[dev.pluginProps[PROP_ZONE]]
			self.debugLog(u"Removed dimmer: " + dev.pluginProps[PROP_ZONE])

	def runConcurrentThread(self):
		self.debugLog(u"Starting serial monitor thread")

		# get initial status of all known devices
		self._sendSerialCommand("LMPI\r")
		self._sendSerialCommand("ZMPI\r")

		try:
			while True:
				s = self.conn.read()
				if self.stopThread:
					self.debugLog(u"Ending serial monitor thread")
					return
				else:
					if len(s) > 0:
						# if we get a '\r' then we should have a complete command
						if s == '\r':
							self._processCommand(self.command)
							self.command = ''
						else:
							self.command += s
		except self.StopThread:
			pass

	def shutdown(self):
		self.debugLog(u"shutdown called")

	def _sendSerialCommand(self, cmd):
		self.debugLog(u"Sending serial command: " + cmd)
		self.conn.write(cmd)

	def _processCommand(self, cmd):
		if cmd.startswith("LMP"):
			self._cmdLEDMap(cmd)
		elif cmd.startswith("ZMP"):
			self._cmdZoneMap(cmd)
		elif cmd.startswith("LZC"):
			self._cmdLocalZoneChange(cmd)
		elif cmd != "!":
			self.debugLog(u"Unknown command: " + cmd)
	
	def _cmdLEDMap(self,cmd):
		map = cmd[4:19];
		self.debugLog(u"Got LED Map: " + map)
		for i, c in enumerate(map):
			id = str(i+1)
			if id in self.phantomButtons:
				but = self.phantomButtons[id]
				if c == '0':
					but.updateStateOnServer("onOffState", False)
				elif c == '1':
					but.updateStateOnServer("onOffState", True)

	def _cmdZoneMap(self, cmd):
		map = cmd[4:36];
		self.debugLog(u"Got Zone Map: " + map)
		for i, c in enumerate(map):
			id = str(i+1)
			if id in self.zones:
				zone = self.zones[id]
				if c == '0':
					zone.updateStateOnServer("onOffState", False)
				elif c == '1':
					zone.updateStateOnServer("onOffState", True)
				if id in self.lastBrightness:
					zone.updateStateOnServer("brightnessLevel", self.lastBrightness[id])
					del self.lastBrightness[id]

	def _cmdLocalZoneChange(self, cmd):
		id = str(int(cmd[4:6]))
		status = cmd[7:10]
		self.debugLog(u"Got Local Zone Change: " + id + " is " + status)
		if id in self.zones:
			zone = self.zones[id]
			if status == "OFF":
				zone.updateStateOnServer("onOffState", False)
			elif status == "ON " or status == "CHG":
				zone.updateStateOnServer("onOffState", True)

	########################################
	# Relay / Dimmer Action callback
	######################
	def actionControlDimmerRelay(self, action, dev):
		###### TURN ON ######
		if action.deviceAction == indigo.kDeviceAction.TurnOn:
			if dev.deviceTypeId == RA_PHANTOM_BUTTON:
				button = dev.pluginProps[PROP_BUTTON]
				self._sendSerialCommand("BP," + button + ",ON\r")
			elif dev.deviceTypeId == RA_DIMMER:
				zone = dev.pluginProps[PROP_ZONE]
				self._sendSerialCommand("SDL," + zone + ",100\r")
				self.lastBrightness[zone] = 100
			indigo.server.log(u"sent \"%s\" %s" % (dev.name, "on"))

		###### TURN OFF ######
		elif action.deviceAction == indigo.kDeviceAction.TurnOff:
			if dev.deviceTypeId == RA_PHANTOM_BUTTON:
				# Command hardware module (dev) to turn OFF here:
				button = dev.pluginProps[PROP_BUTTON]
				self._sendSerialCommand("BP," + button + ",OFF\r")
			elif dev.deviceTypeId == RA_DIMMER:
				zone = dev.pluginProps[PROP_ZONE]
				self._sendSerialCommand("SDL," + zone + ",0\r")
				self.lastBrightness[zone] = 0
			indigo.server.log(u"sent \"%s\" %s" % (dev.name, "off"))

		###### TOGGLE ######
		elif action.deviceAction == indigo.kDeviceAction.Toggle:
			if dev.deviceTypeId == RA_PHANTOM_BUTTON:
				# Command hardware module (dev) to toggle here:
				button = dev.pluginProps[PROP_BUTTON];
				self._sendSerialCommand("BP," + button + ",TOG\r")
			elif dev.deviceTypeId == RA_DIMMER:
				zone = dev.pluginProps[PROP_ZONE]
				if dev.brightness > 0:
					self._sendSerialCommand("SDL," + zone + ",0\r")
				else:
					self._sendSerialCommand("SDL," + zone + ",100\r")
			indigo.server.log(u"sent \"%s\" %s" % (dev.name, "toggle"))

		###### SET BRIGHTNESS ######
		elif action.deviceAction == indigo.kDeviceAction.SetBrightness:
			if dev.deviceTypeId == RA_DIMMER:
				newBrightness = action.actionValue
				zone = dev.pluginProps[PROP_ZONE]
				self._sendSerialCommand("SDL," + zone + "," + str(newBrightness) + "\r")
				self.lastBrightness[zone] = newBrightness
				indigo.server.log(u"sent \"%s\" %s to %d" % (dev.name, "set brightness", newBrightness))

		###### STATUS REQUEST ######
		elif action.deviceAction == indigo.kDeviceAction.RequestStatus:
			if dev.deviceTypeId == RA_PHANTOM_BUTTON:
				self._sendSerialCommand("LMPI\r");
			elif dev.deviceTypeId == RA_DIMMER:
				button = dev.pluginProps[PROP_ZONE];
				self._sendSerialCommand("ZSI," + button + "\r");
			indigo.server.log(u"sent \"%s\" %s" % (dev.name, "status request"))

	def actionAllLightsOn(self, pluginAction):
		self._sendSerialCommand("BP,16,ON\r")
		indigo.server.log(u"sent all lights on action")

	def actionAllLightsOff(self, pluginAction):
		self._sendSerialCommand("BP,17,ON\r")
		indigo.server.log(u"sent all lights off action")